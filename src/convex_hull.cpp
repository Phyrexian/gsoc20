#include <boost/geometry.hpp>
#include <boost/geometry/geometries/geometries.hpp>
#include <iostream>

namespace bg = boost::geometry;

int main() {
    typedef bg::model::point<double, 2, bg::cs::cartesian> point_t;
    typedef bg::model::multi_point<point_t> mpoint_t;

    mpoint_t mpt1;

#if !defined(BOOST_NO_CXX11_UNIFIED_INITIALIZATION_SYNTAX) &&                  \
    !defined(BOOST_NO_CXX11_HDR_INITIALIZER_LIST)

    mpoint_t mpt2{{{0.0, 0.0}, {1.0, 1.0}, {2.0, 2.0}}};

#endif

    bg::append(mpt1, point_t(0.0, 0.0));
    bg::append(mpt1, point_t(1.0, 1.0));
    bg::append(mpt1, point_t(2.0, 2.0));

    std::size_t count = bg::num_points(mpt1);

    std::cout << count << std::endl;

    return 0;
}