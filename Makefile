SRC_DIR = src
IDIR = include
LDIR = lib
ODIR = obj
CC = g++
CFLAGS = -I$(IDIR) -std=c++17 -g -O2 -Wall -Wextra -DLOCAL

_HDR =
HDR = $(patsubst %,$(SRC_DIR)/%,$(_HDR))

_OBJ = convex_hull.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

LIBS =

ifeq ($(OS),Windows_NT)
	PRE =
	SUF = .exe
else
	PRE = ./
	SUF = .out
	LIBS += -lpthread
endif

$(ODIR)/%.o: $(SRC_DIR)/%.cpp $(HDR)
	$(CC) -c -o $@ $< $(CFLAGS)

main: $(OBJ)
	$(CC) -o $@$(SUF) $^ $(CFLAGS) $(LIBS)

.PHONY: clean

run:
	$(PRE)main$(SUF)

clean:
ifeq ($(OS),Windows_NT)
	del /S /Q *.o
else
	rm -f $(ODIR)/*.o *~ core $(IDIR)/*~
endif